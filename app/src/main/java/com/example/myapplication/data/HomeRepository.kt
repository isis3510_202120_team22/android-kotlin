package com.example.myapplication.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.data.DTOs.ScheduledVisitDTO
import com.example.myapplication.data.DTOs.UserLoginDTO
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.data.DTOs.newVisitorDTO
import com.example.myapplication.data.responseDTOs.*
import com.example.myapplication.network.ApiClient
import com.example.myapplication.network.ApiInterface
import com.example.myapplication.ui.login.LoginActivity
import com.example.myapplication.viewmodel.HomeViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeRepository {

    private var apiInterface:ApiInterface?=null

    init {
        apiInterface = ApiClient.getApiClient().create(ApiInterface::class.java)
    }

    fun getUserAuthentication(user: String, hashPassword: String, vm: HomeViewModel, loginActivity: LoginActivity) {
        var data = MutableLiveData<ResponseUserLoginDTO>()
        var body = UserLoginDTO()
        body.inserted_pwd_hash = hashPassword
        apiInterface?.getUserAuthentication(user,body)?.enqueue(object : Callback<ResponseUserLoginDTO>{

            override fun onFailure(call: Call<ResponseUserLoginDTO>, t: Throwable) {
                data.value = ResponseUserLoginDTO("Connection error. Check your Internet access and try again.")
                vm.setUserDto(data, loginActivity)
            }
            override fun onResponse(
                call: Call<ResponseUserLoginDTO>,
                response: Response<ResponseUserLoginDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                    vm.setUserDto(data, loginActivity)
                }
                else if(response.code() == 401){
                    data.value = ResponseUserLoginDTO("The password is not correct. Try again")
                    vm.setUserDto(data, loginActivity)
                }
                else if(response.code() == 404){
                    data.value = ResponseUserLoginDTO("The given user is not found. Try again")
                    vm.setUserDto(data, loginActivity)
                }
            }
        })
    }

    fun getUserAuthentication(user: String, hashPassword: String, vm: HomeViewModel, loginActivity: LoginActivity, loginDataSource: LoginDataSource) {
        var data = MutableLiveData<ResponseUserLoginDTO>()
        var body = UserLoginDTO()
        body.inserted_pwd_hash = hashPassword
        apiInterface?.getUserAuthentication(user,body)?.enqueue(object : Callback<ResponseUserLoginDTO>{

            override fun onFailure(call: Call<ResponseUserLoginDTO>, t: Throwable) {
                data.value = ResponseUserLoginDTO("prueba de fallo")
                vm.setUserDto(data, loginActivity, loginDataSource)
            }
            override fun onResponse(
                call: Call<ResponseUserLoginDTO>,
                response: Response<ResponseUserLoginDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                    vm.setUserDto(data, loginActivity, loginDataSource)
                }else{
                    data.value = ResponseUserLoginDTO("prueba de fallo")
                    vm.setUserDto(data, loginActivity, loginDataSource)
                }
            }
        })
    }

    fun getUnpaidPayments(token: String): LiveData<List<ResponsePaymentDTO>> {
        var data = MutableLiveData<List<ResponsePaymentDTO>>()

        apiInterface?.getUnpaidPayments(token)?.enqueue(object : Callback<List<ResponsePaymentDTO>>{

            override fun onFailure(call: Call<List<ResponsePaymentDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponsePaymentDTO>>,
                response: Response<List<ResponsePaymentDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }
    fun getPayments(token: String): LiveData<List<ResponsePaymentDTO>> {
        var data = MutableLiveData<List<ResponsePaymentDTO>>()

        apiInterface?.getPayments(token)?.enqueue(object : Callback<List<ResponsePaymentDTO>>{

            override fun onFailure(call: Call<List<ResponsePaymentDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponsePaymentDTO>>,
                response: Response<List<ResponsePaymentDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }

    fun addCacheAmount(amount: Int, token: String): LiveData<ResponseCacheAmountDTO> {
        var data = MutableLiveData<ResponseCacheAmountDTO>()

        apiInterface?.addCacheAmount(amount, token)?.enqueue(object : Callback<ResponseCacheAmountDTO>{

            override fun onFailure(call: Call<ResponseCacheAmountDTO>, t: Throwable) {
                data.value = ResponseCacheAmountDTO("prueba de fallo")
            }
            override fun onResponse(
                call: Call<ResponseCacheAmountDTO>,
                response: Response<ResponseCacheAmountDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseCacheAmountDTO("Error")
                }
            }
        })
        return data
    }

    fun addOnline(token: String, value: Int, view: String): LiveData<ResponseOnlineViewDTO>? {
        var data = MutableLiveData<ResponseOnlineViewDTO>()

        apiInterface?.addOnline(token, value, view)?.enqueue(object : Callback<ResponseOnlineViewDTO>{

            override fun onFailure(call: Call<ResponseOnlineViewDTO>, t: Throwable) {
                data.value = ResponseOnlineViewDTO("prueba de fallo")
            }
            override fun onResponse(
                call: Call<ResponseOnlineViewDTO>,
                response: Response<ResponseOnlineViewDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseOnlineViewDTO("Error")
                }
            }
        })
        return data
    }

   //suspend fun getFrequentVisitors(token: String) = apiInterface?.getFrequentVisitors(token)

     fun getFrequentVisitors(token: String): LiveData<List<ResponseVisitorsDTO>> {
        var data = MutableLiveData<List<ResponseVisitorsDTO>>()

        apiInterface?.getFrequentVisitors(token)?.enqueue(object : Callback<List<ResponseVisitorsDTO>>{

            override fun onFailure(call: Call<List<ResponseVisitorsDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseVisitorsDTO>>,
                response: Response<List<ResponseVisitorsDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }



    //suspend fun getLastMonthsVisitors(token: String) = apiInterface?.getLastMonthsVisitors(token)


    fun getLastMonthsVisitors(token: String): LiveData<List<ResponseVisitorsDTO>> {
        var data = MutableLiveData<List<ResponseVisitorsDTO>>()

        apiInterface?.getLastMonthsVisitors(token)?.enqueue(object : Callback<List<ResponseVisitorsDTO>>{

            override fun onFailure(call: Call<List<ResponseVisitorsDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseVisitorsDTO>>,
                response: Response<List<ResponseVisitorsDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }



    //suspend fun getDeleteRecomVisitors(token: String) = apiInterface?.getDeleteRecomVisitors(token)


    fun getDeleteRecomVisitors(token: String): LiveData<List<ResponseVisitorsDTO>> {
        var data = MutableLiveData<List<ResponseVisitorsDTO>>()

        apiInterface?.getDeleteRecomVisitors(token)?.enqueue(object : Callback<List<ResponseVisitorsDTO>>{

            override fun onFailure(call: Call<List<ResponseVisitorsDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseVisitorsDTO>>,
                response: Response<List<ResponseVisitorsDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }



    fun uploadNewVisitor(token: String, visitor: newVisitorDTO): LiveData<Boolean> {
        var data = MutableLiveData<Boolean>();
        apiInterface?.uploadNewVisitor(token, visitor)?.enqueue(object : Callback<ResponseNewVisitorDTO>{

            override fun onFailure(call: Call<ResponseNewVisitorDTO>, t: Throwable) {
                data.value = false;
            }
            override fun onResponse(
                call: Call<ResponseNewVisitorDTO>,
                response: Response<ResponseNewVisitorDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = true;
                }
            }
        })
        return data
    }

    fun createNewResident(resident: newResidentDTO): LiveData<Boolean> {
        var data = MutableLiveData<Boolean>();
        apiInterface?.createNewResident(resident)?.enqueue(object : Callback<ResponseNewResidentDTO>{

            override fun onFailure(call: Call<ResponseNewResidentDTO>, t: Throwable) {
                data.value = false;
            }
            override fun onResponse(
                call: Call<ResponseNewResidentDTO>,
                response: Response<ResponseNewResidentDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = true;
                }
            }
        })
        return data
    }

    fun verifyCode(verificationCode: String): LiveData<Boolean> {
        var data = MutableLiveData<Boolean>();
        apiInterface?.verifyCode(verificationCode)?.enqueue(object : Callback<ResponseVerificationCodeDTO>{

            override fun onFailure(call: Call<ResponseVerificationCodeDTO>, t: Throwable) {
                data.value = false;
            }
            override fun onResponse(
                call: Call<ResponseVerificationCodeDTO>,
                response: Response<ResponseVerificationCodeDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = true;
                }
                else if(response.code() == 404){
                    data.value = false;
                }
            }
        })
        return data
    }

    fun uploadAuthTime(token: String, time: String): LiveData<ResponseAddTimeAuthDTO> {
        var data = MutableLiveData<ResponseAddTimeAuthDTO>()

        apiInterface?.uploadAuthTime(token,time)?.enqueue(object : Callback<ResponseAddTimeAuthDTO>{

            override fun onFailure(call: Call<ResponseAddTimeAuthDTO>, t: Throwable) {
                data.value = ResponseAddTimeAuthDTO("failed login time to DB")
            }
            override fun onResponse(
                call: Call<ResponseAddTimeAuthDTO>,
                response: Response<ResponseAddTimeAuthDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseAddTimeAuthDTO("failed login time to DB")
                }
            }
        })
        return data
    }

    fun uploadCreateResidentTime(time: String): LiveData<ResponseAddTimeCreateResidentDTO> {
        var data = MutableLiveData<ResponseAddTimeCreateResidentDTO>()

        apiInterface?.uploadCreateResidentTime(time)?.enqueue(object : Callback<ResponseAddTimeCreateResidentDTO>{

            override fun onFailure(call: Call<ResponseAddTimeCreateResidentDTO>, t: Throwable) {
                data.value = ResponseAddTimeCreateResidentDTO("failed login time to DB")
            }
            override fun onResponse(
                call: Call<ResponseAddTimeCreateResidentDTO>,
                response: Response<ResponseAddTimeCreateResidentDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseAddTimeCreateResidentDTO("failed login time to DB")
                }
            }
        })
        return data
    }

    fun uploadNotification(token: String, req: String): LiveData<ResponseNotificationDTO> {
        var data = MutableLiveData<ResponseNotificationDTO>()

        apiInterface?.uploadNotification(token,req)?.enqueue(object : Callback<ResponseNotificationDTO>{

            override fun onFailure(call: Call<ResponseNotificationDTO>, t: Throwable) {
                data.value = ResponseNotificationDTO("failed login notification counter to DB")
            }
            override fun onResponse(
                call: Call<ResponseNotificationDTO>,
                response: Response<ResponseNotificationDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseNotificationDTO("failed login notification counter to DB")
                }
            }
        })
        return data
    }

    fun getMonthPrediction(token: String, month: Int): LiveData<ResponseMonthPredictionDTO> {
        var data = MutableLiveData<ResponseMonthPredictionDTO>()

        apiInterface?.getMonthPrediction(token, month)?.enqueue(object : Callback<ResponseMonthPredictionDTO>{

            override fun onFailure(call: Call<ResponseMonthPredictionDTO>, t: Throwable) {
                data.value = ResponseMonthPredictionDTO("Failed to get prediction")
            }
            override fun onResponse(
                call: Call<ResponseMonthPredictionDTO>,
                response: Response<ResponseMonthPredictionDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseMonthPredictionDTO("Failed to get prediction")
                }
            }
        })
        return data
    }

    fun uploadRememberMe(token: String, check: String): LiveData<ResponseRememberMeDTO> {
        var data = MutableLiveData<ResponseRememberMeDTO>()

        apiInterface?.uploadRememberMe(token, check)?.enqueue(object : Callback<ResponseRememberMeDTO>{

            override fun onFailure(call: Call<ResponseRememberMeDTO>, t: Throwable) {
                data.value = ResponseRememberMeDTO("Failed to upload remember me status")
            }
            override fun onResponse(
                call: Call<ResponseRememberMeDTO>,
                response: Response<ResponseRememberMeDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseRememberMeDTO("Failed to upload remember me status")
                }
            }
        })
        return data
    }

    fun postScheduledVisit(token: String, firstName: String, lastName: String, parking: Boolean, date: String, time: String) : LiveData<ResponseScheduledVisit>{
        var data = MutableLiveData<ResponseScheduledVisit>()
        var body = ScheduledVisitDTO(firstName, lastName, parking, date, time)
        apiInterface?.postScheduledVisit(token,body)?.enqueue(object : Callback<ResponseScheduledVisit>{

            override fun onFailure(call: Call<ResponseScheduledVisit>, t: Throwable) {
                data.value = ResponseScheduledVisit("Unable to schedule visit")

            }
            override fun onResponse(
                call: Call<ResponseScheduledVisit>,
                response: Response<ResponseScheduledVisit>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!

                }
                else{
                    data.value = ResponseScheduledVisit("Unable to schedule visit")
                }
            }
        })
        return data
        }

    fun getServices(token: String): LiveData<List<ResponseServiceDTO>> {
        var data = MutableLiveData<List<ResponseServiceDTO>>()

        apiInterface?.getServices(token)?.enqueue(object : Callback<List<ResponseServiceDTO>>{

            override fun onFailure(call: Call<List<ResponseServiceDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseServiceDTO>>,
                response: Response<List<ResponseServiceDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }

    fun getReservations(service: String, date: String): LiveData<List<ResponseReservationDTO>> {
        var data = MutableLiveData<List<ResponseReservationDTO>>()

        apiInterface?.getReservations(service, date)?.enqueue(object : Callback<List<ResponseReservationDTO>>{

            override fun onFailure(call: Call<List<ResponseReservationDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseReservationDTO>>,
                response: Response<List<ResponseReservationDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }

    fun getResidentId(token: String): LiveData<List<ResponseUserIdDTO>> {
        var data = MutableLiveData<List<ResponseUserIdDTO>>()

        apiInterface?.getResidentId(token)?.enqueue(object : Callback<List<ResponseUserIdDTO>>{

            override fun onFailure(call: Call<List<ResponseUserIdDTO>>, t: Throwable) {
                data.value = emptyList()
            }
            override fun onResponse(
                call: Call<List<ResponseUserIdDTO>>,
                response: Response<List<ResponseUserIdDTO>>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = emptyList()
                }
            }
        })
        return data
    }

    fun makeReservation(token: String, service: String, date: String, hour: Int, minute: Int): LiveData<ResponseMakeReservationDTO> {
        var data = MutableLiveData<ResponseMakeReservationDTO>()

        apiInterface?.makeReservation(token, service, date, hour, minute)?.enqueue(object : Callback<ResponseMakeReservationDTO>{

            override fun onFailure(call: Call<ResponseMakeReservationDTO>, t: Throwable) {
                data.value = ResponseMakeReservationDTO("Failed to make reservation")
            }
            override fun onResponse(
                call: Call<ResponseMakeReservationDTO>,
                response: Response<ResponseMakeReservationDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseMakeReservationDTO("Failed to make reservation")
                }
            }
        })
        return data
    }

    fun uploadReservationTime( time: String): LiveData<ResponseAddTimeAuthDTO> {
        var data = MutableLiveData<ResponseAddTimeAuthDTO>()

        apiInterface?.uploadReservationTime(time)?.enqueue(object : Callback<ResponseAddTimeAuthDTO>{

            override fun onFailure(call: Call<ResponseAddTimeAuthDTO>, t: Throwable) {
                data.value = ResponseAddTimeAuthDTO("failed reserve time to DB")
            }
            override fun onResponse(
                call: Call<ResponseAddTimeAuthDTO>,
                response: Response<ResponseAddTimeAuthDTO>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = ResponseAddTimeAuthDTO("failed reserve time to DB")
                }
            }
        })
        return data
    }

    fun getFavorite( token: String): LiveData<String> {
        var data = MutableLiveData<String>()

        apiInterface?.getFavorite(token)?.enqueue(object : Callback<String>{

            override fun onFailure(call: Call<String>, t: Throwable) {
                data.value = "failed reserve time to DB"
            }
            override fun onResponse(
                call: Call<String>,
                response: Response<String>
            ) {
                val res = response.body()
                if (response.code() == 200 &&  res!=null){
                    data.value = res!!
                }
                else{
                    data.value = "failed reserve time to DB"
                }
            }
        })
        return data
    }
}