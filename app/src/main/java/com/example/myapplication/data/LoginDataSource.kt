package com.example.myapplication.data

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.data.model.LoggedInUser
import com.example.myapplication.data.responseDTOs.ResponseUserLoginDTO
import com.example.myapplication.ui.login.LoginActivity
import com.example.myapplication.viewmodel.HomeViewModel
import java.io.IOException
import java.lang.Thread.sleep

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    val MENSAJE_EXITOSO = "Correct Authentication"

    fun login(username: String, password: String, vm: HomeViewModel, loginActivity: LoginActivity){
        try {
            vm.fetchAllPosts(username, password, loginActivity, this)
        } catch (e: Throwable) {
            e.stackTrace
        }
    }

    fun lastLoginStepAuth( vm: HomeViewModel ): Result<LoggedInUser>{
        try {
            // TODO: handle loggedInUser authentication
            var fakeUser = LoggedInUser(java.util.UUID.randomUUID().toString(), "none")
            var detail = "none"
            vm.userDto?.observeForever(Observer {
                if (it != null) {
                    detail = it.detail.toString()
                } else {

                }
            })
            if (detail.equals(MENSAJE_EXITOSO)) {
                fakeUser.displayName = "Exitoso"
                return Result.Success(fakeUser)
            } else {
                return Result.Error(IOException("Error logging in"))
            }
        } catch (e: Throwable) {
            return Result.Error(IOException("Error logging in", e))
        }
    }


    fun logout() {
        // TODO: revoke authentication
    }
}