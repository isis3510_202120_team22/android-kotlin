package com.example.myapplication.data

import com.example.myapplication.data.model.LoggedInUser
import com.example.myapplication.ui.login.LoginActivity
import com.example.myapplication.viewmodel.HomeViewModel

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */

class LoginRepository(val dataSource: LoginDataSource) {

    // in-memory cache of the loggedInUser object
    var user: LoggedInUser? = null
        private set

    private var homeRepository:HomeRepository?=null

    val isLoggedIn: Boolean
        get() = user != null

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
        homeRepository = HomeRepository()
    }

    fun logout() {
        user = null
        dataSource.logout()
    }

    fun login(username: String, password: String, vm: HomeViewModel, loginActivity: LoginActivity): Result<LoggedInUser> {
        // handle login
        dataSource.login(username, password, vm, loginActivity)
        var result = dataSource.lastLoginStepAuth(vm)

        if (result is Result.Success) {
            setLoggedInUser(result.data)
        }

        return result
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }
}