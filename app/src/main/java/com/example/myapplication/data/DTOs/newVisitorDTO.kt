package com.example.myapplication.data.DTOs

data class newVisitorDTO(
    var first_name:String?="",
    var last_name:String?="",
    var phone:String?="",
    var document_type:String?="",
    var document_number:String?="",
    var frequent:Boolean?=false
)