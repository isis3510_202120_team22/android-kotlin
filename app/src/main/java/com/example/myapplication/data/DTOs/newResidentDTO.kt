package com.example.myapplication.data.DTOs

data class newResidentDTO(
    var username: String="",
    var password_hash: String="",
    var first_name:String?="",
    var last_name:String?="",
    var phone:String?="",
    var document_type:String?="",
    var document_id:String?="",
    var code :String?=""
)