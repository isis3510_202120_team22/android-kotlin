package com.example.myapplication.data.responseDTOs

data class ResponseUserLoginDTO(
    var detail:String?="",
    var token:String?=""
)