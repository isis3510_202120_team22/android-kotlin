package com.example.myapplication.data.DTOs

data class ScheduledVisitDTO (
    var first_name:String?="",
    var last_name:String?="",
    var parking:Boolean?=false,
    var date:String?="",
    var time:String?="",
)