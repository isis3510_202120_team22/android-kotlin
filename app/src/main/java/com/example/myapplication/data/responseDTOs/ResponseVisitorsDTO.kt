package com.example.myapplication.data.responseDTOs

data class ResponseVisitorsDTO(
    var first_name:String?="",
    var second_name:String?="",
    var phone:Number?=0,
    var document_type:String?="",
    var document_id:String?=""
)