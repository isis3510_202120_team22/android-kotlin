package com.example.myapplication.data.responseDTOs

data class ResponseReservationDTO (
    var service_id:Int?=0,
    var start_date:String?="",
    var finish_date:String?="",
    var actual_capacity:Int?=0,
    var resident_id:Int?=-1,
)