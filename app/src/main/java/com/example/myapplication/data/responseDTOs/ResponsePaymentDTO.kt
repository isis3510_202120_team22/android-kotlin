package com.example.myapplication.data.responseDTOs

data class ResponsePaymentDTO(
    var payment_id:Int?=0,
    var residence_id:Int?=0,
    var start_date:String?="",
    var due_date:String?="",
    var status:String?="",
    var concept:String?="",
    var amount:Double?=0.0,
    var payment_date:String?=null
)