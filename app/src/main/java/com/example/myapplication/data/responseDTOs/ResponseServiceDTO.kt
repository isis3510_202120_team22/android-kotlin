package com.example.myapplication.data.responseDTOs

data class ResponseServiceDTO (
    var service_id:Int?=0,
    var condo_id:Int?=0,
    var capacity:Int?=0,
    var name:String?="",
    var duration:Int?=0,
    var additional_info:String?="",
)