package com.example.myapplication.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.data.DTOs.newVisitorDTO
import com.example.myapplication.data.HomeRepository
import com.example.myapplication.data.LoginDataSource
import com.example.myapplication.data.responseDTOs.*
import com.example.myapplication.ui.login.LoginActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(application: Application): AndroidViewModel(application){

        private var homeRepository:HomeRepository?=null
        var userDto : LiveData<ResponseUserLoginDTO>?=null
        var listUnpaidPayments:LiveData<List<ResponsePaymentDTO>>?=null
        var listPayments:LiveData<List<ResponsePaymentDTO>>?=null
        var listFrequentVisitors:LiveData<List<ResponseVisitorsDTO>>?=null
        var listLastMonthsVisitors:LiveData<List<ResponseVisitorsDTO>>?=null
        var getDeleteRecomVisitors:LiveData<List<ResponseVisitorsDTO>>?=null
        var responseAddTimeAuthDTO:LiveData<ResponseAddTimeAuthDTO>?=null
        var responseAddTimeCreateResidentDTO:LiveData<ResponseAddTimeCreateResidentDTO>?=null
        var responseNotificationDTO:LiveData<ResponseNotificationDTO>?=null
        var responseMonthPredictionDTO:LiveData<ResponseMonthPredictionDTO>?=null
        var responseRememberMeDTO:LiveData<ResponseRememberMeDTO>?=null
        var responseScheduledVisit:LiveData<ResponseScheduledVisit>?=null
        var uploadedNewVisitor : LiveData<Boolean>?=null
        var createdNewVisitor : LiveData<Boolean>?=null
        var verifiedCode : LiveData<Boolean>?=null
        var loaded : Boolean?=null
        var cacheAmount : LiveData<ResponseCacheAmountDTO>?=null
        var isOnline: LiveData<ResponseOnlineViewDTO>?=null
        var listServices:LiveData<List<ResponseServiceDTO>>?=null
        var listReservations:LiveData<List<ResponseReservationDTO>>?=null
        var listUserId:LiveData<List<ResponseUserIdDTO>>?=null
        var responseMakeReservation:LiveData<ResponseMakeReservationDTO>?=null
        var responseAddTimeReserveDTO:LiveData<ResponseAddTimeAuthDTO>?=null
        var favorite:LiveData<String>?=null

        init {
            homeRepository = HomeRepository()
            userDto = MutableLiveData()
            listUnpaidPayments = MutableLiveData()
            listPayments = MutableLiveData()
            listFrequentVisitors = MutableLiveData()
            responseAddTimeAuthDTO = MutableLiveData()
            responseAddTimeCreateResidentDTO = MutableLiveData()
            responseNotificationDTO = MutableLiveData()
            responseMonthPredictionDTO = MutableLiveData()
            responseRememberMeDTO = MutableLiveData()
            responseScheduledVisit = MutableLiveData()
            uploadedNewVisitor = MutableLiveData()
            createdNewVisitor = MutableLiveData()
            verifiedCode = MutableLiveData()
            cacheAmount = MutableLiveData()
            isOnline = MutableLiveData()
            getDeleteRecomVisitors = MutableLiveData()
            listLastMonthsVisitors = MutableLiveData()
            listServices = MutableLiveData()
            listReservations = MutableLiveData()
            listUserId = MutableLiveData()
            responseMakeReservation = MutableLiveData()
            responseAddTimeReserveDTO = MutableLiveData()
            favorite = MutableLiveData()
            loaded = false
        }

        fun fetchAllPosts(user: String, password: String, loginActivity: LoginActivity, loginDataSource: LoginDataSource){
            homeRepository?.getUserAuthentication(user, password,this, loginActivity, loginDataSource)
            loaded = false
        }

        fun fetchAllPosts(user: String, password: String, loginActivity: LoginActivity){
            homeRepository?.getUserAuthentication(user, password,this, loginActivity)
            loaded = false
        }

        @JvmName("setUserDto1")
        fun setUserDto(user : LiveData<ResponseUserLoginDTO>, loginActivity: LoginActivity, loginDataSource: LoginDataSource){
            userDto = user
            loginActivity.finalLoginAuth()
            loginDataSource.lastLoginStepAuth(this)
        }

        @JvmName("setUserDto1")
        fun setUserDto(user : LiveData<ResponseUserLoginDTO>, loginActivity: LoginActivity){
            userDto = user
            loginActivity.finalLoginAuth()
        }

        fun getUnpaidPayments(token: String){
            listUnpaidPayments = homeRepository?.getUnpaidPayments(token)
        }

        fun getPayments(token: String){
            listPayments = homeRepository?.getPayments(token)
        }

        fun addCacheAmount(amount: Int, token: String){
            cacheAmount = homeRepository?.addCacheAmount(amount, token)
        }

        fun addOnline(token: String, value: Int, view: String) {
            isOnline = homeRepository?.addOnline(token, value, view)
        }

         fun getFrequentVisitors(token: String){
             listFrequentVisitors = homeRepository?.getFrequentVisitors(token)
        }


/*
        fun getFrequentVisitors(token: String){
            CoroutineScope(Dispatchers.IO).launch {
                val response = homeRepository?.getFrequentVisitors(token)
                withContext(Dispatchers.Main) {
                    if (response != null) {
                        if (response.isSuccessful) {
                            listFrequentVisitors?.postValue(response.body())
                        }
                    }
                }
            }

        }

 */


         fun getLastMonthsVisitors(token: String){
             listLastMonthsVisitors = homeRepository?.getLastMonthsVisitors(token)
        }



/*
        fun getLastMonthsVisitors(token: String){
            CoroutineScope(Dispatchers.IO).launch {
                val response = homeRepository?.getLastMonthsVisitors(token)
                withContext(Dispatchers.Main) {
                    if (response != null) {
                        if (response.isSuccessful) {
                            listLastMonthsVisitors?.postValue(response.body())
                        }
                    }
                }
            }

        }


 */


         fun getDeleteRecomVisitors(token: String){
             getDeleteRecomVisitors = homeRepository?.getDeleteRecomVisitors(token)
        }




/*
        fun getDeleteRecomVisitors(token: String){
            CoroutineScope(Dispatchers.IO).launch {
                val response = homeRepository?.getDeleteRecomVisitors(token)
                withContext(Dispatchers.Main) {
                    if (response != null) {
                        if (response.isSuccessful) {
                            getDeleteRecomVisitors?.postValue(response.body())
                        }
                    }
                }
            }

        }


 */

        fun updloadNewVisitor(token: String, visitor: newVisitorDTO){
            uploadedNewVisitor = homeRepository?.uploadNewVisitor(token, visitor)
        }

        fun verifyCode(verificationCode: String){
            verifiedCode = homeRepository?.verifyCode(verificationCode)
        }

        fun createNewResident(resident: newResidentDTO){
            createdNewVisitor = homeRepository?.createNewResident(resident)
        }

        fun uploadAuthTime(token: String, time: String){
            responseAddTimeAuthDTO = homeRepository?.uploadAuthTime(token,time)
        }

        fun uploadCreateResidentTime(time: String){
            responseAddTimeCreateResidentDTO = homeRepository?.uploadCreateResidentTime(time)
        }

        fun uploadNotification(token: String, req: String){
            responseNotificationDTO = homeRepository?.uploadNotification(token,req)
        }

        fun getMonthPrediction(token: String, month: Int){
            responseMonthPredictionDTO = homeRepository?.getMonthPrediction(token, month)
        }

        fun uploadRememberMe(token: String, check: String){
            responseRememberMeDTO = homeRepository?.uploadRememberMe(token, check)
        }

        fun postScheduledVisit(token:String, firstName: String, secondName: String, parking: Boolean, date: String, time: String){
            responseScheduledVisit = homeRepository?.postScheduledVisit(token, firstName, secondName, parking, date, time)
        }

        fun getServices(token: String){
            listServices = homeRepository?.getServices(token)
        }

        fun getReservations(service: String, date: String){
            listReservations = homeRepository?.getReservations(service,date)
        }

        fun getResidentId(token: String){
            listUserId = homeRepository?.getResidentId(token)
        }

        fun makeReservation(token:String, service: String, date: String, hour: Int, minute: Int){
            responseMakeReservation = homeRepository?.makeReservation(token, service, date, hour, minute)
        }

        fun uploadReserveTime( time: String){
            responseAddTimeReserveDTO = homeRepository?.uploadReservationTime(time)
        }

        fun getFavorite( token: String){
            favorite = homeRepository?.getFavorite(token)
        }

    /**
     *
     *
     *
     *
    fun createPost(userDto: UserLoginDTO){
    createPostLiveData = homeRepository?.createPost(userDto)
    }

    fun deletePost(id:Int){
    deletePostLiveData = homeRepository?.deletePost(id)
    }
     */

    }