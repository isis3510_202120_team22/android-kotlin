package com.example.myapplication.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.viewmodel.HomeViewModel

class NotificationActivity : AppCompatActivity(){
    private lateinit var vm: HomeViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_after_notification)
        val token = intent.extras?.get("token") as String
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        vm.uploadNotification(token, "open")
        val mIntent = Intent(this@NotificationActivity, PaymentsActivity::class.java)
        mIntent.putExtra("token", token)
        mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        startActivity(mIntent)
        finish()
    }
}