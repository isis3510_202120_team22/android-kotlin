package com.example.myapplication.ui.login

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment


class RetryDialogFragment : DialogFragment(){

    internal lateinit var listener: RetryDialogListener



    interface RetryDialogListener {

        fun onDialogPositiveClick(dialog: DialogFragment)
        fun onDialogNegativeClick(dialog: DialogFragment)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {

            listener = context as RetryDialogListener
        } catch (e: ClassCastException) {

            throw ClassCastException((context.toString() +
                    " must implement RetryDialogListener"))
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            builder.setMessage("You do not have an Internet connection. Please connect to the Internet and try again.")
                .setTitle("No Connection!")
                .setPositiveButton(
                    "Try again",
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onDialogPositiveClick(this)
                    })
                .setNegativeButton("Okay",
                    DialogInterface.OnClickListener { dialog, id ->
                        listener.onDialogNegativeClick(this)
                    })
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}