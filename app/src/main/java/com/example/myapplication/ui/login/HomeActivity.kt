package com.example.myapplication.ui.login

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.ActivityHomeBinding
import com.example.myapplication.R
import com.example.myapplication.viewmodel.HomeViewModel
import kotlinx.coroutines.*

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding
    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: NotificationCompat.Builder
    private val channelId = "i.apps.notifications"
    private val description = "Test notification"
    private lateinit var vm: HomeViewModel
    private lateinit var token: String
    private lateinit var connectionTextView: TextView
    private lateinit var scope: CoroutineScope

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        // accessing button
        val imageView: ImageView = (binding.cardImage1)
        imageView.setImageResource(R.drawable.booking)
        val imageView2: ImageView = (binding.cardImage2)
        imageView2.setImageResource(R.drawable.dayoff)
        val imageView3: ImageView = (binding.cardImage3)
        imageView3.setImageResource(R.drawable.payonline)
        connectionTextView = binding.connection!!
        val paymentsButton: ImageButton = (binding.homeButton5)
        val paymentsButton2: Button = (binding.cardButtonPayment)
        val visitorsButton: ImageButton = (binding.homeButton2)
        val visitorsButton2: Button = (binding.cardButtonVisitors)
        val servicesButton: ImageButton = (binding.homeButton3)
        val servicesButton2: Button = (binding.cardButtonServices)
        token = intent.extras?.get("token") as String



        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        vm.getUnpaidPayments(token)
        vm.listUnpaidPayments?.observe(this, Observer {
            val dto = it ?: return@Observer
            if(dto.isNotEmpty()){
                val intent = Intent(this, NotificationActivity::class.java)
                intent.putExtra("token", token)
                val pendingIntent : PendingIntent? = TaskStackBuilder.create(this).run {
                    addNextIntentWithParentStack(intent)
                    getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                }
                vm.uploadNotification(token, "notified")
                // RemoteViews are used to use the content of
                // some different layout apart from the current activity layout
                //val contentView = RemoteViews(packageName, R.layout.activity_after_notification)

                // checking if android version is greater than oreo(API 26) or not
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationChannel = NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
                    notificationChannel.enableLights(true)
                    notificationChannel.lightColor = Color.GREEN
                    notificationChannel.enableVibration(false)
                    notificationManager.createNotificationChannel(notificationChannel)

                    builder = NotificationCompat.Builder(this, channelId)
                        .setContentText("There are some unpaid payments close to expire!")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_launcher_background))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                } else {

                    builder = NotificationCompat.Builder(this)
                        .setContentText("There are some unpaid payments close to expire!")
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setLargeIcon(BitmapFactory.decodeResource(this.resources, R.drawable.ic_launcher_background))
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                }
                notificationManager.notify(1234, builder.build())
            }
        })

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        paymentsButton2.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        visitorsButton.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }
        visitorsButton2.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        servicesButton.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }
        servicesButton2.setOnClickListener{
            val mIntent = Intent(this@HomeActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume()  {
        super.onResume()

        var sharedPref : SharedPreferences = getPreferences(MODE_PRIVATE)
        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("HOME-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            connectionOn()
            vm.addOnline(token, 1, "home")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "home")
            }
            editor.putInt("HOME-OFFLINE-COUNTER", 0)
        }
        else{
            connectionOff()
            editor.putInt("HOME-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()

        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch { checkConnection() }

    }

    override fun onStop() {
        super.onStop()
        scope.cancel()
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)

            if (isOnline(applicationContext)){
                connectionOn()
            }
            else{
                connectionOff()
            }
        }
    }

    private fun connectionOn(){
        GlobalScope.launch {
        withContext(Dispatchers.Main) {
            connectionTextView.text = getString(R.string.connected)
            connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }
}