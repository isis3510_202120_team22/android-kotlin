package com.example.myapplication.ui.login

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityHomeBinding
import com.example.myapplication.databinding.ActivityLoginBinding
import com.example.myapplication.viewmodel.HomeViewModel

import android.content.Intent


import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import kotlinx.coroutines.*
import java.math.BigInteger
import java.security.MessageDigest
import android.widget.Toast





class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var binding: ActivityLoginBinding
    private lateinit var homescreen:ActivityHomeBinding
    private lateinit var vm: HomeViewModel
    private var initial: Long = 0
    private lateinit var checkBox: CheckBox
    private lateinit var username: EditText
    private lateinit var password: EditText
    private lateinit var sharedPref: SharedPreferences
    private lateinit var user: String
    private lateinit var pass: String
    private lateinit var connectionTextView: TextView
    private lateinit var scope: CoroutineScope

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        homescreen = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setContentView(homescreen.root)
        vm = ViewModelProvider(this)[HomeViewModel::class.java]

        username = binding.username
        password = binding.password
        val login = binding.login
        val loading = binding.loading
        val createAccount: TextView? = (binding.createAccount)
        checkBox = binding.checkBoxRemember!!
        connectionTextView = binding.connection!!
        sharedPref = getPreferences(MODE_PRIVATE)

        user = sharedPref.getString("USER_NAME", "No user")!!
        pass = sharedPref.getString("USER_PASSWORD", "No pass")!!

        if(user != "No user"){
            username.setText(user)
            password.setText(pass)
            checkBox.isChecked = true
        }


        loginViewModel = ViewModelProvider(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            /*
            login.isEnabled = loginState.isDataValid
            */


            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
            val loginResult = it ?: return@Observer

            //loading.visibility = View.GONE
            if (loginResult.error != null) {
                showLoginFailed(loginResult.error)
            }
            if (loginResult.success != null) {
                updateUiWithUser(loginResult.success)
            }
            setResult(Activity.RESULT_OK)

            //Complete and destroy login activity once successful
            finish()
        })

        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            md5(password.text.toString(), pass == password.text.toString()),
                            vm,
                            this@LoginActivity
                        )
                }
                false
            }

            login?.setOnClickListener {
                if (loading != null) {
                    loading.visibility = View.VISIBLE
                }
                initial = System.currentTimeMillis()
                vm.fetchAllPosts(username.text.toString(), md5(password.text.toString(), pass == password.text.toString()), this@LoginActivity)

            }

            createAccount?.setOnClickListener{
                val mIntent = Intent(this@LoginActivity, CreateResidentActivity::class.java)
                startActivity(mIntent)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch { checkConnection() }
    }

     fun finalLoginAuth(){
        vm.userDto?.observe(this@LoginActivity, Observer {
            val dto = it ?: return@Observer
            Toast.makeText(
                applicationContext,
                "${dto.detail}",
                Toast.LENGTH_LONG
            ).show()
            if (dto.detail.equals("Correct authentication") && dto.token!== ""){
                val editor = sharedPref.edit()
                val check: String
                if(checkBox.isChecked){
                    editor.putString("USER_NAME", username.text.toString())
                    editor.putString("USER_PASSWORD", md5(password.text.toString(), pass == password.text.toString()))
                    editor.apply()
                    check = "rememberyes"
                }
                else {
                    editor.remove("USER_NAME")
                    editor.remove("USER_PASSWORD")
                    editor.apply()
                    check = "rememberno"
                }
                vm.uploadRememberMe(dto.token.toString(), check)

                val final = System.currentTimeMillis()
                val duration = final - initial
                val strDuration = ((duration/1000).toString() + "." + duration/100)
                vm.uploadAuthTime(dto.token.toString(), strDuration)

                val mIntent = Intent(this@LoginActivity, HomeActivity::class.java)
                mIntent.putExtra("token", dto.token)
                startActivity(mIntent)
            }
        })
    }

    private fun updateUiWithUser(model: LoggedInUserView) {
        val welcome = getString(R.string.welcome)
        val displayName = model.displayName
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)

            if (isOnline(applicationContext)){
                connectionOn()
            }
            else{
                connectionOff()
            }
        }
    }

    suspend private fun connectionOn(){
        withContext(Dispatchers.Main) {
            connectionTextView.text = getString(R.string.connected)
            connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

fun md5(input:String, makeHash: Boolean): String {
    if(makeHash){
        return input

    }
    val md = MessageDigest.getInstance("MD5")
    return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
}

@RequiresApi(Build.VERSION_CODES.M)
fun isOnline(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val capabilities =
        connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    if (capabilities != null) {
        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
            return true
        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
            return true
        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
            Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
            return true
        }
    }

    return false
}

