package com.example.myapplication.ui.login

import NewResidentLruCache
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.viewmodel.HomeViewModel
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.data.responseDTOs.ResponseVisitorsDTO
import com.example.myapplication.databinding.ActivityRegisterResidentBinding
import kotlinx.coroutines.*


class CreateResidentActivity: AppCompatActivity(), RetryDialogFragment.RetryDialogListener, CoroutineScope by MainScope() {
    private lateinit var createResidentBinding: ActivityRegisterResidentBinding
    private lateinit var vm: HomeViewModel
    private var newResident = newResidentDTO();
    private lateinit var lruCache : NewResidentLruCache
    private lateinit var sharedPref: SharedPreferences
    private lateinit var ver_code: String
    private lateinit var scope: CoroutineScope
    private lateinit var connectionTextView: TextView
    private lateinit var continueButton: Button

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createResidentBinding = ActivityRegisterResidentBinding.inflate(layoutInflater)
        setContentView(createResidentBinding.root)
        lruCache = NewResidentLruCache()
        sharedPref = getPreferences(MODE_PRIVATE)

        val backButtom: ImageButton = (createResidentBinding.backButton1)

        val nameInput: EditText = (createResidentBinding.nameInput2)
        val idtypeInput: Spinner = (createResidentBinding.spinnerInput2)
        val idNumberInput: EditText = (createResidentBinding.idNumberInput2)
        val phoneInput: EditText = (createResidentBinding.phoneInput2)
        val verificationCodeInput: EditText = (createResidentBinding.verificationCodeInput)
        connectionTextView = createResidentBinding.connection!!
        continueButton = (createResidentBinding.checkDataBtn)

        val filledAllFields = IntArray(4);
        for (field in filledAllFields){
            filledAllFields[field] = 0
        }

        vm = ViewModelProvider(this)[HomeViewModel::class.java]

        ver_code = sharedPref.getString("VERIFICATION_CODE", "No code")!!

        if(ver_code != "No code" && filledAllFields.isEmpty()){
            filledAllFields.set(3,1);
            verificationCodeInput.setText(ver_code)
        }

        if(lruCache.retrieveNewResidentFromCache("new")!= null){
            newResident = lruCache.retrieveNewResidentFromCache("new")!!
            if(newResident.toString() != ""){
                if(newResident.first_name?:"" != ""){
                    filledAllFields.set(0,1);
                    var full_name = newResident.first_name?:""
                    if(newResident.last_name != ""){
                        full_name += " " + newResident.last_name
                    }
                    nameInput.setText(full_name)
                }
                if(newResident.document_id?:"" != "") {
                    filledAllFields.set(1,1);
                    idNumberInput.setText(newResident.document_id)
                }
                if(newResident.phone?:"" != "") {
                    filledAllFields.set(2,1);
                    phoneInput.setText(newResident.phone)
                }
            }
        }

        nameInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(0,1);
                    newResident.first_name = s.toString().trim({ it <= ' ' }).split(" ")[0]
                    try {
                        newResident.last_name = s.toString().trim({ it <= ' ' }).split(" ")[1]
                    }
                    catch (e: Exception){
                        // Do nothing
                    }
                    lruCache.saveNewResidentToCache("new", newResident)
                }
                else{
                    filledAllFields.set(0,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        idNumberInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(1,1);
                    newResident.document_id = s.toString().trim({ it <= ' ' })
                    lruCache.saveNewResidentToCache("new", newResident)
                }
                else{
                    filledAllFields.set(1,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        phoneInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(2,1);
                    newResident.phone = s.toString().trim({ it <= ' ' })
                    lruCache.saveNewResidentToCache("new", newResident)
                }
                else{
                    filledAllFields.set(2,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        verificationCodeInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(3,1);
                    newResident.code = s.toString().trim({ it <= ' ' })
                    val editor = sharedPref.edit()
                    editor.putString("VERIFICATION_CODE", newResident.code)
                    editor.apply()
                }
                else{
                    filledAllFields.set(3,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        val spinner: Spinner = (createResidentBinding.spinnerInput2)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.IDTypes,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        backButtom.setOnClickListener{
            val mIntent = Intent(this@CreateResidentActivity, LoginActivity::class.java)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        continueButton.setOnClickListener{
        if(!filledAllFields.contains(0)){

            val nameFilled = nameInput.text.toString();
            val idTypeFilled = idtypeInput.selectedItem.toString();
            val idNumberFilled = idNumberInput.text.toString();
            val phoneFilled = phoneInput.text.toString();
            val verificationCodeFilled = verificationCodeInput.text.toString();

            newResident.first_name = try { nameFilled.split(" ")[0] } catch (e: IndexOutOfBoundsException) { nameFilled };
            newResident.last_name = try { nameFilled.split(" ")[1] } catch (e: IndexOutOfBoundsException) { "" };
            newResident.phone = phoneFilled;
            newResident.document_type = idTypeFilled;
            newResident.document_id = idNumberFilled;
            newResident.code = verificationCodeFilled;

            if (isOnline(this)){
                verifyCode(verificationCodeFilled)
            }
            else{
                val newFragment = RetryDialogFragment()
                newFragment.show(supportFragmentManager, "No connection")
            }
        }
        else {
            Toast.makeText(
                applicationContext,
                "Please fill in all the fields to continue",
                Toast.LENGTH_LONG
            ).show()
        }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        if(isOnline(this)){
            connectionOn()
        }
        else{
            connectionOff()
        }
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch {
            checkConnection() }
    }

    private fun verifyCode(verificationCode: String){
        lruCache.saveNewResidentToCache("new", newResident)
        vm.verifyCode(verificationCode)
        vm.verifiedCode?.observe(this, Observer {
            val dto = it ?: return@Observer
            if (dto) {
                val mIntent = Intent(this@CreateResidentActivity, CreateResidentActivity2::class.java)
                mIntent.putExtra("Resident", newResident.toString())
                startActivity(mIntent)
                finish()
            } else {
                Toast.makeText(
                    applicationContext,
                    "Opps! It seems that your verification code is not valid. Please check",
                    Toast.LENGTH_LONG).show()
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDialogPositiveClick(dialog: DialogFragment) {
        if (isOnline(this)){
            newResident.code?.let { verifyCode(it) }
        }
        else{
            val newFragment = RetryDialogFragment()
            newFragment.show(supportFragmentManager, "No connection")
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){
                connectionOn()
            }
            else{
                connectionOff()
            }
        }
    }

    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }

}