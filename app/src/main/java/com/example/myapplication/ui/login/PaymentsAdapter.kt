package com.example.myapplication.ui.login


import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat.startActivityForResult

import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponsePaymentDTO
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class PaymentsAdapter(var listener:PaymentsActivity) : RecyclerView.Adapter<PaymentsAdapter.HomeViewHolder>(){

    private var data : ArrayList<ResponsePaymentDTO>?=null

    interface HomeListener{
        fun onItemDeleted(postModel: ResponsePaymentDTO, position: Int)
    }

    fun setData(list: ArrayList<ResponsePaymentDTO>){
        data = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        return HomeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_detail, parent, false), listener)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val item = data?.get(position)
        holder.bindView(item)

    }


    class HomeViewHolder(itemView: View, payments: PaymentsActivity) : RecyclerView.ViewHolder(itemView){
        val pays = payments
        fun bindView(item: ResponsePaymentDTO?) {
            var concept : TextView= itemView.findViewById(R.id.concept)
            concept.text = item?.concept

            var amount : TextView= itemView.findViewById(R.id.amount)
            amount.text = "Amount: " + item?.amount.toString()

            var duedate : TextView= itemView.findViewById(R.id.duedate)
            duedate.text = "Due date: " + item?.due_date



            item?.due_date?.let {
                val proximity = getColorDate(it)
                var linearL : LinearLayout= itemView.findViewById(R.id.linearL)


                if (proximity < 1){
                    linearL.setBackgroundResource(R.drawable.proximity_color_urgent)
                }
                else if (proximity < 3){
                    linearL.setBackgroundResource(R.drawable.proximity_color_close)
                }
                else{
                    linearL.setBackgroundResource(R.drawable.proximity_color_far)
                }


            }
            val REQUEST_IMAGE_CAPTURE = 1

            var btnUpload : Button = itemView.findViewById(R.id.card_button3)
            btnUpload.setOnClickListener{
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                try {
                    startActivityForResult(pays , takePictureIntent, REQUEST_IMAGE_CAPTURE, null)
                } catch (e: ActivityNotFoundException) {
                    // display error state to the user

                }

            }

            fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
                if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    //imageView.setImageBitmap(imageBitmap)
                }
            }



        }


        fun getColorDate(date: String): Long{
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = sdf.parse(sdf.format(Date()))
            val dueDate = sdf.parse(date)

            val diff: Long = dueDate.time - currentDate.time
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24

            return days
        }

    }




}