package com.example.myapplication.ui.login


import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponseVisitorsDTO


class VisitorsAdapter(var listener:VisitorsActivity, var token:String) : RecyclerView.Adapter<VisitorsAdapter.HomeViewHolder>(){

    private var data : ArrayList<ResponseVisitorsDTO>?=null

    interface HomeListener{
        fun onItemDeleted(postModel: ResponseVisitorsDTO, position: Int)
    }

    fun setData(list: ArrayList<ResponseVisitorsDTO>){
        data = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        return HomeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.visitors_detail, parent, false), listener, token)
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val item = data?.get(position)
        holder.bindView(item)
    }


    class HomeViewHolder(itemView: View, visitors: VisitorsActivity, token:String) : RecyclerView.ViewHolder(itemView){
        val visitors = visitors
        val token = token
        fun bindView(item: ResponseVisitorsDTO?) {
            var visitors_name : TextView= itemView.findViewById(R.id.visitorsName)
            visitors_name.text = item?.first_name + " " +  item?.second_name
            var layout: LinearLayout = itemView.findViewById(R.id.linearL)

            val SCHEDULE_VISIT = 1

            layout.setOnClickListener{

                val scheduleVisitIntent = Intent(visitors, ScheduleVisitActivity::class.java)
                try {
                    scheduleVisitIntent.putExtra("token", token)
                    scheduleVisitIntent.putExtra("firstName", item?.first_name)
                    scheduleVisitIntent.putExtra("secondName", item?.second_name)
                    ActivityCompat.startActivityForResult(visitors, scheduleVisitIntent,SCHEDULE_VISIT, null)
                } catch (e: ActivityNotFoundException) {
                    // display error state to the user

                }
            }

            fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
                if (requestCode == SCHEDULE_VISIT && resultCode == Activity.RESULT_OK) {

                }
            }
        }
    }
}