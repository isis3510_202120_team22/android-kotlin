package com.example.myapplication.ui.login

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponseReservationDTO
import java.text.SimpleDateFormat
import android.content.DialogInterface
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.myapplication.viewmodel.HomeViewModel


class ReservationAdapter(var listener:ReserveServiceActivity, var token:String, var service:String, var residentId: Int,var date:String, var vm: HomeViewModel): RecyclerView.Adapter<ReservationAdapter.HomeViewHolder>() {
    private var data : ArrayList<ResponseReservationDTO>?=null

    interface HomeListener{
        fun onItemDeleted(postModel: ResponseReservationDTO, position: Int)
    }

    fun setData(list: ArrayList<ResponseReservationDTO>){
        data = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReservationAdapter.HomeViewHolder {
        return ReservationAdapter.HomeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.reservation_detail, parent, false),
            listener, token, service, residentId, date, vm
        )
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: ReservationAdapter.HomeViewHolder, position: Int) {
        val item = data?.get(position)
        holder.bindView(item)

    }

    class HomeViewHolder(itemView: View,  reservations: ReserveServiceActivity, token:String, service: String, residentId: Int, date:String, vm: HomeViewModel) : RecyclerView.ViewHolder(itemView){
        val reservations = reservations
        val service = service
        val residentId = residentId
        val vm = vm
        var token = token
        var date = date
        fun bindView(item: ResponseReservationDTO?) {

            val startDate = item?.start_date
            val finishDate = item?.finish_date
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val sdf2 = SimpleDateFormat("hh:mm a")
            val data1 = sdf.parse(startDate)
            val hora = sdf2.format(data1)
            val data2 = sdf.parse(finishDate)
            val hora2 = sdf2.format(data2)

            var dateText : TextView = itemView.findViewById(R.id.time)
            dateText.text = hora + " to " + hora2

            var image : ImageView = itemView.findViewById(R.id.serviceIcon)
            var imageType = ""
            if(service == "Pool"){
                image.setImageResource(R.drawable.pool)
            }
            else if(service == "BBQ Floor 1"){
                image.setImageResource(R.drawable.bbq)
            }
            else if(service == "BBQ Floor 2"){
                image.setImageResource(R.drawable.bbq)
            }
            else if(service == "Videogames"){
                image.setImageResource(R.drawable.gaming)
            }
            else if(service == "Billar"){
                image.setImageResource(R.drawable.billar)
            }
            else if(service == "Gym"){
                image.setImageResource(R.drawable.gym)
            }
            
            var stateText : TextView = itemView.findViewById(R.id.state)
            val resId = item?.resident_id
            val layout: LinearLayout = itemView.findViewById(R.id.reservationLayout)
            if(resId == null)
            {
                stateText.text = "Available"
                stateText.setTextColor(ContextCompat.getColor(reservations,R.color.proximity_far) )
                layout.setOnClickListener{
                    AlertDialog.Builder(reservations)
                        .setTitle("Confirm Reservation")
                        .setMessage("Please confirm your reservation for: $service")
                        .setPositiveButton("Confirm",
                            DialogInterface.OnClickListener { dialog, which ->
                                val initial = System.currentTimeMillis()
                                vm.makeReservation(token, service, date, data1.hours, data1.minutes)
                                vm.responseMakeReservation?.observe(reservations, Observer {
                                    val dto = it ?: return@Observer
                                    if(dto.detail!="Failed to make reservation"){
                                        Toast.makeText(
                                            reservations,
                                            service+" succesfully reserved!",
                                            Toast.LENGTH_LONG
                                        ).show()
                                        val final = System.currentTimeMillis()
                                        val duration = final - initial
                                        val strDuration = ((duration/1000).toString() + "." + duration/100)
                                        vm.uploadReserveTime(strDuration)
                                        reservations.finish()
                                    }
                                    else{
                                        Toast.makeText(
                                            reservations,
                                            "Reservation failed. Try again.",
                                            Toast.LENGTH_LONG
                                        ).show()

                                    }
                                })
                            })
                        .setNegativeButton("Cancel", null)
                        .show()
                }
            }
            else if (resId == residentId){
                stateText.text = "Reserved by you"
                stateText.setTextColor(ContextCompat.getColor(reservations,R.color.black) )
                layout.setOnClickListener(null)
            }
            else{
                stateText.text = "Not available"
                stateText.setTextColor(ContextCompat.getColor(reservations,R.color.proximity_urgent) )
                layout.setOnClickListener(null)
            }


        }
    }
}