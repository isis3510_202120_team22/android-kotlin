package com.example.myapplication.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.R
import com.example.myapplication.databinding.ActivityAddVisitorBinding
import com.example.myapplication.viewmodel.HomeViewModel
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.example.myapplication.data.DTOs.newVisitorDTO
import com.example.myapplication.data.responseDTOs.ResponseVisitorsDTO


class AddVisitorsActivity: AppCompatActivity(), RetryDialogFragment.RetryDialogListener {
    private lateinit var addVisitorsBinding: ActivityAddVisitorBinding
    private lateinit var vm: HomeViewModel
    lateinit var token: String
    private val newVisitor = newVisitorDTO();

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addVisitorsBinding = ActivityAddVisitorBinding.inflate(layoutInflater)
        setContentView(addVisitorsBinding.root)

        val backButtom: ImageButton = (addVisitorsBinding.backButton1)

        val visitorsButton: ImageButton = (addVisitorsBinding.homeButton2)
        val homeButton: ImageButton = (addVisitorsBinding.homeButton4)
        val paymentsButton: ImageButton = (addVisitorsBinding.homeButton5)
        val servicesButton: ImageButton = (addVisitorsBinding.homeButton3)
        val addVisitorButton: Button = (addVisitorsBinding.addVisitorsButton)
        val nameInput: EditText = (addVisitorsBinding.nameInput)
        val idtypeInput: Spinner = (addVisitorsBinding.spinnerInput)
        val idNumberInput: EditText = (addVisitorsBinding.idNumberInput)
        val phoneInput: EditText = (addVisitorsBinding.phoneInput)
        val isFrequentInput: CheckBox = (addVisitorsBinding.checkBoxFrequentVisitor)
        val filledAllFields = IntArray(3);
        for (field in filledAllFields){
            filledAllFields[field] = 0
        }
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        token = intent.extras?.get("token") as String

        nameInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(0,1);
                }
                else{
                    filledAllFields.set(0,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        idNumberInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(1,1);
                }
                else{
                    filledAllFields.set(1,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        phoneInput.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(2,1);
                }
                else{
                    filledAllFields.set(2,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })

        val spinner: Spinner = (addVisitorsBinding.spinnerInput)
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter.createFromResource(
            this,
            R.array.IDTypes,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            spinner.adapter = adapter
        }

        backButtom.setOnClickListener{
            val mIntent = Intent(this@AddVisitorsActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        addVisitorButton.setOnClickListener{
        if(!filledAllFields.contains(0)){

            val nameFilled = nameInput.text.toString();
            val idTypeFilled = idtypeInput.selectedItem.toString();
            val idNumberFilled = idNumberInput.text.toString();
            val phoneFilled = phoneInput.text.toString();
            val isFrequentVisitor = isFrequentInput.isChecked();

            newVisitor.first_name = try { nameFilled.split(" ")[0] } catch (e: IndexOutOfBoundsException) { nameFilled };
            newVisitor.last_name = try { nameFilled.split(" ")[1] } catch (e: IndexOutOfBoundsException) { "" };
            newVisitor.phone = phoneFilled;
            newVisitor.document_type = idTypeFilled;
            newVisitor.document_number = idNumberFilled;
            newVisitor.frequent = isFrequentVisitor;

            if (isOnline(this)){
                addVisitor(newVisitor)
            }
            else{
                val newFragment = RetryDialogFragment()
                newFragment.show(supportFragmentManager, "No connection")
            }
        }
        else {
            Toast.makeText(
                applicationContext,
                "Please fill in all the fields to create the visitor",
                Toast.LENGTH_LONG
            ).show()
        }
        }



        /**
         * Here comes the buttom bar
         */

        visitorsButton.setOnClickListener{
            val mIntent = Intent(this@AddVisitorsActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        homeButton.setOnClickListener{
            val mIntent = Intent(this@AddVisitorsActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@AddVisitorsActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }

        servicesButton.setOnClickListener{
            val mIntent = Intent(this@AddVisitorsActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent);
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        var sharedPref : SharedPreferences = getPreferences(MODE_PRIVATE)
        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("ADD_VISITOR-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            vm.addOnline(token, 1, "add_visitor")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "add_visitor")
            }
            editor.putInt("ADD_VISITOR-OFFLINE-COUNTER", 0)
        }
        else {
            editor.putInt("ADD_VISITOR-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
    }

    private fun addVisitor(newVisitor: newVisitorDTO){
        vm.updloadNewVisitor(token, newVisitor);
        vm.uploadedNewVisitor?.observe(this, Observer {
            val dto = it ?: return@Observer
            if (dto) {
                Toast.makeText(
                    applicationContext,
                    "Visitor successfully created!",
                    Toast.LENGTH_LONG
                ).show()
                finish()
            } else {
                Toast.makeText(
                    applicationContext,
                    "Opps! There was an error at creating the visitor. Please try again",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDialogPositiveClick(dialog: DialogFragment) {
        if (isOnline(this)){
            addVisitor(newVisitor)
        }
        else{
            val newFragment = RetryDialogFragment()
            newFragment.show(supportFragmentManager, "No connection")
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }

}