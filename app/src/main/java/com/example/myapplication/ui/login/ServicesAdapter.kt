package com.example.myapplication.ui.login

import android.content.ActivityNotFoundException
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout

import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponseServiceDTO

import kotlin.collections.ArrayList

class ServicesAdapter(var listener:ServicesActivity, var token:String, var favorite:String): RecyclerView.Adapter<ServicesAdapter.HomeViewHolder>() {
    private var data : ArrayList<ResponseServiceDTO>?=null

    interface HomeListener{
        fun onItemDeleted(postModel: ResponseServiceDTO, position: Int)
    }

    fun setData(list: ArrayList<ResponseServiceDTO>){
        data = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ServicesAdapter.HomeViewHolder {
        return ServicesAdapter.HomeViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.service_detail, parent, false),
            listener,token, favorite
        )
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    override fun onBindViewHolder(holder: ServicesAdapter.HomeViewHolder, position: Int) {
        val item = data?.get(position)
        holder.bindView(item)

    }

    class HomeViewHolder(itemView: View, services: ServicesActivity, token: String, favorite: String) : RecyclerView.ViewHolder(itemView){
        val services = services
        val token = token
        var favorite = favorite
        fun bindView(item: ResponseServiceDTO?) {
            var name : TextView = itemView.findViewById(R.id.serviceName)
            if(favorite==item?.name){
                name.text = "Favorite: "+ item?.name
                name.setTextColor(ContextCompat.getColor(services,R.color.proximity_urgent) )
            }else{
                name.text = item?.name
            }


            var image : ImageView = itemView.findViewById(R.id.serviceImage)
            var imageType = item?.name
            if(item?.name?.let { it?.contains("Pool", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.pool)
                imageType = "Pool"
            }
            else if(item?.name?.let { it?.contains("BBQ Floor 1", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.bbq)
                imageType = "BBQ Floor 1"
            }
            else if(item?.name?.let { it?.contains("BBQ Floor 2", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.bbq)
                imageType = "BBQ Floor 2"
            }
            else if(item?.name?.let { it?.contains("videogames", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.gaming)
                imageType = "Videogames"
            }
            else if(item?.name?.let { it?.contains("billar", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.billar)
                imageType = "Billar"
            }
            else if(item?.name?.let { it?.contains("gym", ignoreCase = true) } == true){
                image.setImageResource(R.drawable.gym)
                imageType = "Gym"
            }

            var layout: LinearLayout = itemView.findViewById(R.id.serviceLayout)
            layout.setOnClickListener{

                val reserveServiceIntent = Intent(services, ReserveServiceActivity::class.java)
                try {
                    reserveServiceIntent.putExtra("token", token)
                    reserveServiceIntent.putExtra("service", imageType)
                    ActivityCompat.startActivity(services, reserveServiceIntent, null)
                } catch (e: ActivityNotFoundException) {
                    // display error state to the user

                }
            }
        }
    }
}
