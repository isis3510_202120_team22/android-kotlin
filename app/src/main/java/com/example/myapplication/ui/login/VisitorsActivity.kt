package com.example.myapplication.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponseVisitorsDTO
import com.example.myapplication.databinding.ActivityVisitorsBinding
import com.example.myapplication.network.VisitorsLruCache
import com.example.myapplication.viewmodel.HomeViewModel
import kotlinx.coroutines.*

class VisitorsActivity: AppCompatActivity(), VisitorsAdapter.HomeListener, CoroutineScope by MainScope() {

    private lateinit var visitorsBinding: ActivityVisitorsBinding
    private lateinit var vm: HomeViewModel
    private lateinit var adapter1: VisitorsAdapter
    private lateinit var adapter2: VisitorsAdapter
    private lateinit var adapter3: VisitorsToDeleteAdapter
    private lateinit var recycler1: RecyclerView
    private lateinit var recycler2: RecyclerView
    private lateinit var recycler3: RecyclerView
    private lateinit var token: String
    private lateinit var lruCache: VisitorsLruCache
    private lateinit var addVisitorsButton: ImageButton
    private lateinit var connectionTextView: TextView
    private lateinit var scope: CoroutineScope
    private var change: Boolean = false


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        visitorsBinding = ActivityVisitorsBinding.inflate(layoutInflater)
        setContentView(visitorsBinding.root)
        recycler1 = findViewById(R.id.recycler1)
        recycler2 = findViewById(R.id.recycler2)
        recycler3 = findViewById(R.id.recycler3)
        addVisitorsButton = visitorsBinding.addVisitorsButton
        val backButton: ImageButton = (visitorsBinding.backButton1)
        connectionTextView = visitorsBinding.connection!!
        val homeButton: ImageButton = (visitorsBinding.homeButton4)
        val paymentsButton: ImageButton = (visitorsBinding.homeButton5)
        val servicesButton: ImageButton = (visitorsBinding.homeButton3)
        token = intent.extras?.get("token") as String

        lruCache = VisitorsLruCache()

        initAdapter()

        vm = ViewModelProvider(this)[HomeViewModel::class.java]




        addVisitorsButton.setOnClickListener{
            val mIntent = Intent(this@VisitorsActivity, AddVisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            startActivity(mIntent)
        }

        backButton.setOnClickListener{
            val mIntent = Intent(this@VisitorsActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        /**
         * Here comes the button bar
         */

        homeButton.setOnClickListener{
            val mIntent = Intent(this@VisitorsActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@VisitorsActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }


        servicesButton.setOnClickListener{
            val mIntent = Intent(this@VisitorsActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        var sharedPref : SharedPreferences = getPreferences(MODE_PRIVATE)
        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("VISITORS-OFFLINE-COUNTER", 0)

        if(isOnline(this)){
            connectionOn()
            loadData()
            change = true
            //launch{
                vm.addOnline(token, 1, "visitors")
                for(i: Int in 0 until offlineCounter){
                    vm.addOnline(token, 0, "visitors")
                }
           // }

            editor.putInt("VISITORS-OFFLINE-COUNTER", 0)
        }
        else{
            connectionOff()
            change = false
            val frequentVisitorsCacheArray: ArrayList<ResponseVisitorsDTO?> = ArrayList()
            for (i in 0 until lruCache.getFrequentVisitorsSize()){
                frequentVisitorsCacheArray.add(lruCache.retrieveFrequentVisitorsFromCache(i.toString()))
            }
            recycler1.visibility = View.VISIBLE
            adapter1.setData(frequentVisitorsCacheArray as ArrayList<ResponseVisitorsDTO>)

            val visitorsCacheArray: ArrayList<ResponseVisitorsDTO?> = ArrayList()
            for (i in 0 until lruCache.getVisitorsSize()){
                visitorsCacheArray.add(lruCache.retrieveVisitorsFromCache(i.toString()))
            }
            recycler2.visibility = View.VISIBLE
            adapter2.setData(visitorsCacheArray as ArrayList<ResponseVisitorsDTO>)

            editor.putInt("VISITORS-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch {

            checkConnection() }

    }

    override fun onStop() {
        super.onStop()
        scope.cancel()
    }

    private fun initAdapter() {
        adapter1 = VisitorsAdapter(this, token)
        adapter2 = VisitorsAdapter(this, token)
        adapter3 = VisitorsToDeleteAdapter(this, token)
        recycler1.layoutManager = LinearLayoutManager(this)
        recycler1.adapter = adapter1
        recycler2.layoutManager = LinearLayoutManager(this)
        recycler2.adapter = adapter2
        recycler3.layoutManager = LinearLayoutManager(this)
        recycler3.adapter = adapter3
    }

    override fun onItemDeleted(postModel: ResponseVisitorsDTO, position: Int) {
        TODO("Not yet implemented")
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun loadData(){
            addVisitorsButton.isEnabled = true

            vm.getFrequentVisitors(token)
            vm.getLastMonthsVisitors(token)
            vm.getDeleteRecomVisitors(token)


            vm.listFrequentVisitors?.observe(this, Observer {
                val dto = it ?: return@Observer
                if(dto.isNotEmpty()){
                    recycler1.visibility = View.VISIBLE
                    adapter1.setData(it as ArrayList<ResponseVisitorsDTO>)
                    val counter: Int = if (dto.size > 3) {
                        2
                    } else{
                        dto.size - 1
                    }
                    for (i in 0..counter){
                        lruCache.saveFrequentVisitorsToCache(i.toString(), dto[i])
                    }
                }
                else{
                    Toast.makeText(
                        applicationContext,
                        "Oops! It seems that you don't have Frequent visitors registered.",
                        Toast.LENGTH_LONG
                    ).show()

                }
            })


            vm.listLastMonthsVisitors?.observe(this, Observer {
                val dto = it ?: return@Observer
                if(dto.isNotEmpty()){
                    recycler2.visibility = View.VISIBLE
                    adapter2.setData(it as ArrayList<ResponseVisitorsDTO>)
                    val counter: Int = if (dto.size > 5) {
                        4
                    } else{
                        dto.size - 1
                    }
                    for (i in 0..counter){
                        lruCache.saveVisitorsToCache(i.toString(), dto[i])
                    }
                }
                else{
                    Toast.makeText(
                        applicationContext,
                        "Oops! It seems that you don't have previous visitors ",
                        Toast.LENGTH_LONG
                    ).show()

                }
            })


            vm.getDeleteRecomVisitors?.observe(this, Observer {
                val dto = it ?: return@Observer
                if(dto.isNotEmpty()){
                    recycler3.visibility = View.VISIBLE
                    adapter3.setData(it as ArrayList<ResponseVisitorsDTO>)
                    val counter: Int = if (dto.size > 5) {
                        4
                    } else{
                        dto.size - 1
                    }
                    for (i in 0..counter){
                        lruCache.saveVisitorsToCache(i.toString(), dto[i])
                    }
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){

                connectionOn()
                if(!change){
                    change = true
                    withContext(Dispatchers.Main) {
                        loadData()

                    }
                }
            }
            else{
                connectionOff()
                if (change){
                    change = false
                }
            }
        }
    }

    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
                addVisitorsButton.isEnabled = false
            }
        }
    }

}