package com.example.myapplication.ui.login

import PaymentsLruCache
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponsePaymentDTO
import com.example.myapplication.databinding.ActivityPaymentsBinding
import com.example.myapplication.viewmodel.HomeViewModel
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class PaymentsActivity: AppCompatActivity(), PaymentsAdapter.HomeListener {
    private lateinit var paymentsBinding: ActivityPaymentsBinding
    private lateinit var vm: HomeViewModel
    private lateinit var adapter: PaymentsAdapter
    private lateinit var recycler: RecyclerView
    private val REQUEST_IMAGE_CAPTURE = 1
    lateinit var token: String
    private lateinit var sharedPref: SharedPreferences
    private lateinit var connectionTextView: TextView
    private lateinit var lruCache : PaymentsLruCache
    private lateinit var scope: CoroutineScope
    private var change: Boolean = false


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        paymentsBinding = ActivityPaymentsBinding.inflate(layoutInflater)
        val visitorsButton: ImageButton = (paymentsBinding.homeButton2)
        val homeButton: ImageButton = (paymentsBinding.homeButton4)
        val servicesButton: ImageButton = (paymentsBinding.homeButton3)
        setContentView(paymentsBinding.root)
        connectionTextView = paymentsBinding.connection!!
        recycler = findViewById(R.id.recycler)
        sharedPref = getPreferences(MODE_PRIVATE)
        initAdapter()
        lruCache = PaymentsLruCache()
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        token = intent.extras?.get("token") as String



        homeButton.setOnClickListener{
            val mIntent = Intent(this@PaymentsActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        visitorsButton.setOnClickListener{
            val mIntent = Intent(this@PaymentsActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        servicesButton.setOnClickListener{
            val mIntent = Intent(this@PaymentsActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("PAYMENTS-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            connectionOn()
            loadData(editor)
            change = true
            vm.addOnline(token, 1, "payments")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "payments")
            }
            editor.putInt("PAYMENTS-OFFLINE-COUNTER", 0)
        }
        else{
            connectionOff()
            change = false
            val dtoCacheArray: ArrayList<ResponsePaymentDTO?> = ArrayList()

            for (i in 0 until lruCache.getRealSize1()){
                dtoCacheArray.add(lruCache.retrievePaymentsFromCache(i.toString()))
            }
            recycler.visibility = View.VISIBLE
            adapter.setData(dtoCacheArray as ArrayList<ResponsePaymentDTO>)

            val predict = sharedPref.getString("USER-PREDICTION", "No")
            if(predict != "No")
            {
                changeMonthPrediction(predict)
            }
            editor.putInt("PAYMENTS-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch { checkConnection() }
    }

    override fun onStop() {
        super.onStop()
        scope.cancel()
    }

    private fun initAdapter() {
        adapter = PaymentsAdapter(this)
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = adapter
    }

    private fun changeMonthPrediction(monthPaid: String?){
        val textView: TextView = findViewById(R.id.textView4)
        textView.text = "Predicted payments: $$monthPaid"
    }

    override fun onItemDeleted(postModel: ResponsePaymentDTO, position: Int) {
        TODO("Not yet implemented")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            //imageView.setImageBitmap(imageBitmap)

            vm.uploadNotification(token, "camera")
        }
    }

    private fun loadData(editor: SharedPreferences.Editor){
        vm.getPayments(token)
        vm.listPayments?.observe(this, Observer {
            val dto = it ?: return@Observer
            if(dto.isNotEmpty()){
                recycler.visibility = View.VISIBLE
                adapter.setData(it as ArrayList<ResponsePaymentDTO>)
                val counter: Int = if (dto.size > 10) {
                    9
                } else{
                    dto.size - 1
                }
                vm.addCacheAmount(counter+1, token)
                for (i in 0..counter){
                    lruCache.savePaymentsToCache(i.toString(), dto[i])
                }
            }
            else{
                Toast.makeText(
                    applicationContext,
                    "No registered payments found.",
                    Toast.LENGTH_LONG
                ).show()

            }
        })

        val month = Calendar.getInstance().get(Calendar.MONTH) + 1

        vm.getMonthPrediction(token, month)
        vm.responseMonthPredictionDTO?.observe(this, Observer {
            val dto = it ?: return@Observer
            if(!dto.prediction.equals("Failed to get prediction")){
                val rounded:String = String.format("%.2f", dto.prediction?.toDouble())
                changeMonthPrediction(rounded)
                editor.putString("USER-PREDICTION", rounded)
            }
            else{
                changeMonthPrediction("0.0")
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){
                connectionOn()
                if(!change){
                    change = true
                    withContext(Dispatchers.Main) {
                        loadData(sharedPref.edit())
                    }
                }
            }
            else{
                connectionOff()
                if (change){
                    change = false
                }
            }
        }
    }

    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }
}