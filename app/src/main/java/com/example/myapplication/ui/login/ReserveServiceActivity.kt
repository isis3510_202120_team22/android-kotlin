package com.example.myapplication.ui.login

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.responseDTOs.ResponseReservationDTO
import com.example.myapplication.data.responseDTOs.ResponseServiceDTO
import com.example.myapplication.databinding.ActivityReserveServiceBinding

import com.example.myapplication.network.ServicesLruCache
import com.example.myapplication.viewmodel.HomeViewModel
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class ReserveServiceActivity: AppCompatActivity(), DatePickerFragment.dateSelectedListener {

    private lateinit var reserveBinding: ActivityReserveServiceBinding
    private lateinit var vm: HomeViewModel
    private lateinit var adapter: ReservationAdapter
    private lateinit var recycler: RecyclerView
    lateinit var token: String
    lateinit var service: String
    lateinit var date: String
    private lateinit var sharedPref: SharedPreferences
    private lateinit var connectionTextView: TextView
    //private lateinit var lruCache : ServicesLruCache
    private lateinit var scope: CoroutineScope
    private var change: Boolean = false
    private lateinit var dateText: TextView

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reserveBinding = ActivityReserveServiceBinding.inflate(layoutInflater)
        val visitorsButton: ImageButton = (reserveBinding.homeButton2)
        val homeButton: ImageButton = (reserveBinding.homeButton4)
        val paymentsButton: ImageButton = (reserveBinding.homeButton5)
        val backButton: ImageButton = (reserveBinding.backButton1)
        setContentView(reserveBinding.root)
        connectionTextView = reserveBinding.connection!!
        recycler = findViewById(R.id.recyclerReservations)
        sharedPref = getPreferences(MODE_PRIVATE)

        //lruCache = ServicesLruCache()
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        token = intent.extras?.get("token") as String
        service = intent.extras?.get("service") as String
        val title: TextView = reserveBinding.textView5
        title.text = service

        initAdapter()

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)+1
        val day = c.get(Calendar.DAY_OF_MONTH)

        var month1 = ""
        if(month < 10){
            month1="0"
        }
        var day1 = ""

        if(day < 10){
            day1="0"
        }



        date = "$year-$month1$month-$day1$day"
        dateText = reserveBinding.textView9
        dateText.text = date

        backButton.setOnClickListener{
            finish()
        }


        homeButton.setOnClickListener{
            val mIntent = Intent(this@ReserveServiceActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        visitorsButton.setOnClickListener{
            val mIntent = Intent(this@ReserveServiceActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@ReserveServiceActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        dateText.setOnClickListener{
            val newFragment = DatePickerFragment()
            val dateString = date.split("-")
            var args : Bundle = Bundle()
            args.putInt("year", dateString[0].toInt())
            args.putInt("month", dateString[1].toInt()-1)
            args.putInt("day", dateString[2].toInt())
            newFragment.arguments = args
            newFragment.show(supportFragmentManager, "datePicker")
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("RESERVATIONS-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            connectionOn()
            loadData()
            change = true
            vm.addOnline(token, 1, "reservations")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "reservations")
            }
            editor.putInt("RESERVATIONS-OFFLINE-COUNTER", 0)
        }
        else{
            connectionOff()
            change = false
            /*

            val dtoCacheArray: ArrayList<ResponseServiceDTO?> = ArrayList()

            for (i in 0 until lruCache.getRealSize1()){
                dtoCacheArray.add(lruCache.retrieveServicesFromCache(i.toString()))
            }
            recycler.visibility = View.VISIBLE
            adapter.setData(dtoCacheArray as ArrayList<ResponseReservationDTO>)



             */


            editor.putInt("RESERVATIONS-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch { checkConnection() }
    }



    override fun onStop() {
        super.onStop()
        scope.cancel()
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun initAdapter() {
        var residentId: Int
        if(isOnline(this)){
            vm.getResidentId(token)
            vm.listUserId?.observe(this, Observer{
                val dto = it ?: return@Observer
                if(dto.isNotEmpty()){
                    residentId = dto[0].resident_id!!
                    adapter = ReservationAdapter(this, token, service, residentId, date, vm)
                    recycler.layoutManager = LinearLayoutManager(this)
                    recycler.adapter = adapter
                }
            })
        }else{
            residentId = -2
            adapter = ReservationAdapter(this, token, service, residentId, date, vm)
            recycler.layoutManager = LinearLayoutManager(this)
            recycler.adapter = adapter
        }

    }



    private fun loadData(){
        vm.getReservations(service, date)
        vm.listReservations?.observe(this, Observer{
            val dto = it ?: return@Observer
            if(dto.isNotEmpty()){
                recycler.visibility = View.VISIBLE
                adapter.setData(it as ArrayList<ResponseReservationDTO>)
/*
                val counter: Int = if (dto.size > 8) {
                    7
                } else{
                    dto.size - 1
                }
                vm.addCacheAmount(counter+1, token)
                for (i in 0..counter){
                    lruCache.saveServicesToCache(i.toString(), dto[i])
                }


 */

            }
            else{
                Toast.makeText(
                    applicationContext,
                    "No registered reservations found.",
                    Toast.LENGTH_LONG
                ).show()
                adapter.setData(ArrayList<ResponseReservationDTO>())
            }
        })
    }



    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }


    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){
                connectionOn()
                if(!change){
                    change = true
                    withContext(Dispatchers.Main) {
                        loadData()
                    }
                }
            }
            else{
                connectionOff()
                if (change){
                    change = false
                }
            }
        }
    }



    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun dateSelected(year: Int, month: Int, day:Int) {
        var month1 = ""
        if(month < 10){
            month1="0"
        }
        var day1 = ""

        if(day < 10){
            day1="0"
        }
        date = "$year-$month1$month-$day1$day"
        dateText.text = date
        initAdapter()
        loadData()
    }

}

