package com.example.myapplication.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager

import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

import androidx.lifecycle.Observer
import com.example.myapplication.data.responseDTOs.ResponseServiceDTO
import com.example.myapplication.databinding.ActivityServicesBinding
import com.example.myapplication.network.ServicesLruCache
import com.example.myapplication.viewmodel.HomeViewModel
import kotlinx.coroutines.*
import java.util.*
import kotlin.collections.ArrayList

class ServicesActivity: AppCompatActivity() {

    private lateinit var servicesBinding: ActivityServicesBinding
    private lateinit var vm: HomeViewModel
    private lateinit var adapter: ServicesAdapter
    private lateinit var recycler: RecyclerView
    lateinit var token: String
    private lateinit var sharedPref: SharedPreferences
    private lateinit var connectionTextView: TextView
    private lateinit var lruCache : ServicesLruCache
    private lateinit var scope: CoroutineScope
    private var change: Boolean = false

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        servicesBinding = ActivityServicesBinding.inflate(layoutInflater)
        val visitorsButton: ImageButton = (servicesBinding.homeButton2)
        val homeButton: ImageButton = (servicesBinding.homeButton4)
        val paymentsButton: ImageButton = (servicesBinding.homeButton5)
        val backButton: ImageButton = (servicesBinding.backButton1)
        setContentView(servicesBinding.root)
        connectionTextView = servicesBinding.connection!!
        recycler = findViewById(R.id.recyclerServices)
        sharedPref = getPreferences(MODE_PRIVATE)

        lruCache = ServicesLruCache()
        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        token = intent.extras?.get("token") as String

        initAdapter()
        backButton.setOnClickListener{
            val mIntent = Intent(this@ServicesActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }


        homeButton.setOnClickListener{
            val mIntent = Intent(this@ServicesActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        visitorsButton.setOnClickListener{
            val mIntent = Intent(this@ServicesActivity, VisitorsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@ServicesActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("SERVICES-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            connectionOn()
            loadData()
            change = true
            vm.addOnline(token, 1, "services")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "services")
            }
            editor.putInt("SERVICES-OFFLINE-COUNTER", 0)
        }
        else{
            connectionOff()
            change = false

            val dtoCacheArray: ArrayList<ResponseServiceDTO?> = ArrayList()

            for (i in 0 until lruCache.getRealSize1()){
                dtoCacheArray.add(lruCache.retrieveServicesFromCache(i.toString()))
            }
            recycler.visibility = View.VISIBLE
            adapter.setData(dtoCacheArray as ArrayList<ResponseServiceDTO>)




            editor.putInt("SERVICES-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch { checkConnection() }
    }

    override fun onStop() {
        super.onStop()
        scope.cancel()
    }



    @RequiresApi(Build.VERSION_CODES.M)
    private fun initAdapter() {
        if(isOnline(this)){
            vm.getFavorite(token)
            vm.favorite?.observe(this, Observer {
                val dto = it ?: return@Observer
                if(dto != ""){
                    adapter = ServicesAdapter(this, token, dto)
                    recycler.layoutManager = GridLayoutManager(this,2)
                    recycler.adapter = adapter
                }

            })
        }
        else{
            adapter = ServicesAdapter(this, token, "")
            recycler.layoutManager = GridLayoutManager(this,2)
            recycler.adapter = adapter
        }


    }


    private fun loadData(){
        vm.getServices(token)
        vm.listServices?.observe(this, Observer{
            val dto = it ?: return@Observer
            if(dto.isNotEmpty()){
                recycler.visibility = View.VISIBLE
                adapter.setData(it as ArrayList<ResponseServiceDTO>)

                val counter: Int = if (dto.size > 8) {
                    7
                } else{
                    dto.size - 1
                }
                vm.addCacheAmount(counter+1, token)
                for (i in 0..counter){
                    lruCache.saveServicesToCache(i.toString(), dto[i])
                }


            }
            else{
                Toast.makeText(
                    applicationContext,
                    "No registered services found.",
                    Toast.LENGTH_LONG
                ).show()

            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                return true
            } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                return true
            }
        }
        return false
    }


    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){
                connectionOn()
                if(!change){
                    change = true
                    withContext(Dispatchers.Main) {
                        loadData()
                    }
                }
            }
            else{
                connectionOff()
                if (change){
                    change = false
                }
            }
        }
    }


    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext,R.color.proximity_urgent) )
            }
        }
    }

}