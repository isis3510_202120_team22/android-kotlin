package com.example.myapplication.ui.login

import NewResidentLruCache
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.viewmodel.HomeViewModel
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import com.example.myapplication.R
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.databinding.ActivityRegisterResident2Binding
import kotlinx.coroutines.*
import java.math.BigInteger
import java.security.MessageDigest


class CreateResidentActivity2: AppCompatActivity(), RetryDialogFragment.RetryDialogListener, CoroutineScope by MainScope()  {
    private lateinit var createResidentBinding2: ActivityRegisterResident2Binding
    private lateinit var vm: HomeViewModel
    private var newResident = newResidentDTO();
    private var initial: Long = 0
    private lateinit var lruCache : NewResidentLruCache
    private lateinit var sharedPref: SharedPreferences
    private lateinit var ver_code: SharedPreferences
    private lateinit var scope: CoroutineScope
    private lateinit var connectionTextView: TextView
    private lateinit var createAccount: Button

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createResidentBinding2 = ActivityRegisterResident2Binding.inflate(layoutInflater)
        setContentView(createResidentBinding2.root)
        lruCache = NewResidentLruCache()
        sharedPref = getPreferences(MODE_PRIVATE)

        newResident = lruCache.retrieveNewResidentFromCache("new")!!

        val userName: EditText = (createResidentBinding2.userName2)
        val password: EditText = (createResidentBinding2.passwordInput2)
        connectionTextView = createResidentBinding2.connection!!
        createAccount = (createResidentBinding2.createAccountBtn)

        val filledAllFields = IntArray(2);
        for (field in filledAllFields){
            filledAllFields[field] = 0
        }
        vm = ViewModelProvider(this)[HomeViewModel::class.java]

        ver_code = getSharedPreferences("VERIFICATION_CODE", Context.MODE_PRIVATE)
        sharedPref = ver_code

        userName.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(0,1);
                }
                else{
                    filledAllFields.set(0,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })
        password.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
                if (!s.toString().trim({ it <= ' ' }).isEmpty())
                {
                    filledAllFields.set(1,1);
                }
                else{
                    filledAllFields.set(1,0);
                }
            }
            override fun beforeTextChanged(s:CharSequence, start:Int, count:Int,
                                           after:Int) {
                // TODO Auto-generated method stub
            }
            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        })


        createAccount.setOnClickListener{
        if(!filledAllFields.contains(0)){

            val userNameFilled = userName.text.toString();
            val passwordFilled = password.text.toString();

            if(ver_code.contains(newResident.code)){
                newResident.code = sharedPref.getString("VERIFICATION_CODE", "No code")!!
            }

            newResident.username = userNameFilled;
            newResident.password_hash = md5(passwordFilled, false);
            println(newResident)
            if (isOnline(this)){
                createResident(newResident)
            }
            else{
                val newFragment = RetryDialogFragment()
                newFragment.show(supportFragmentManager, "No connection. Try later")
            }
        }
        else {
            Toast.makeText(
                applicationContext,
                "Please fill username & password fields to continue",
                Toast.LENGTH_LONG
            ).show()
        }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()

        if(isOnline(this)){
            connectionOn()
        }
        else{
            connectionOff()
        }
        scope = CoroutineScope(Job() + Dispatchers.Default)
        scope.launch {
            checkConnection() }
    }

    private fun createResident(newResident: newResidentDTO){
        initial = System.currentTimeMillis()
        vm.createNewResident(newResident);
        vm.createdNewVisitor?.observe(this, Observer {
            val dto = it ?: return@Observer
            if (dto) {
                val final = System.currentTimeMillis()
                val duration = final - initial
                val strDuration = ((duration/1000).toString() + "." + duration/100)
                vm.uploadCreateResidentTime(strDuration)
                var resetResident = newResidentDTO();
                lruCache.saveNewResidentToCache("new", resetResident)
                val editor = ver_code.edit()
                editor.putString("VERIFICATION_CODE", "No code")
                editor.apply()
                Toast.makeText(
                    applicationContext,
                    "Resident successfully created! Now you can login",
                    Toast.LENGTH_LONG
                ).show()
                val mIntent = Intent(this@CreateResidentActivity2, LoginActivity::class.java)
                startActivity(mIntent)
                finish()
            } else {
                Toast.makeText(
                    applicationContext,
                    "Opps! There was an error at creating the resident. Please try again",
                    Toast.LENGTH_LONG
                ).show()
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDialogPositiveClick(dialog: DialogFragment) {
        if (isOnline(this)){
            createResident(newResident)
        }
        else{
            val newFragment = RetryDialogFragment()
            newFragment.show(supportFragmentManager, "No connection")
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }

    fun md5(input:String, makeHash: Boolean): String {
        if(makeHash){
            return input

        }
        val md = MessageDigest.getInstance("MD5")
        return BigInteger(1, md.digest(input.toByteArray())).toString(16).padStart(32, '0')
    }

    @RequiresApi(Build.VERSION_CODES.M)
    suspend fun checkConnection () = withContext(Dispatchers.Default){

        while (true){
            delay(3_000)
            if (isOnline(applicationContext)){
                connectionOn()
            }
            else{
                connectionOff()
            }
        }
    }

    private fun connectionOn(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext, R.color.proximity_far) )
            }
        }
    }

    private fun connectionOff(){
        GlobalScope.launch {
            withContext(Dispatchers.Main) {
                connectionTextView.text = getString(R.string.not_connected)
                connectionTextView.setTextColor(ContextCompat.getColor(applicationContext, R.color.proximity_urgent) )
            }
        }
    }
}