package com.example.myapplication.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.ActivityScheduleVisitBinding
import com.example.myapplication.viewmodel.HomeViewModel
import java.text.SimpleDateFormat



class ScheduleVisitActivity: AppCompatActivity(), RetryDialogFragment.RetryDialogListener {

    private lateinit var currentDate: String
    private var currentMinute: Int = 0
    private var currentHour: Int = 0
    private lateinit var currentTime: String
    private var parking: Boolean = false
    private lateinit var scheduleVisitBinding: ActivityScheduleVisitBinding
    private lateinit var token: String
    private lateinit var firstName: String
    private lateinit var secondName: String
    private lateinit var vm: HomeViewModel

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scheduleVisitBinding = ActivityScheduleVisitBinding.inflate(layoutInflater)
        setContentView(scheduleVisitBinding.root)
        token = intent.extras?.get("token") as String
        firstName = intent.extras?.get("firstName") as String
        secondName = intent.extras?.get("secondName") as String

        val homeButton: ImageButton = (scheduleVisitBinding.homeButton4)
        val paymentsButton: ImageButton = (scheduleVisitBinding.homeButton5)
        val visitorsButton: ImageButton = (scheduleVisitBinding.homeButton2)
        val backButton: ImageButton = (scheduleVisitBinding.backButton1)
        val servicesButton: ImageButton = (scheduleVisitBinding.homeButton3)

        vm = ViewModelProvider(this)[HomeViewModel::class.java]
        val textName: TextView = scheduleVisitBinding.textView6
        textName.text = firstName + " " + secondName

        val calendarView : CalendarView = scheduleVisitBinding.calendarView
        val timePicker: TimePicker = scheduleVisitBinding.timePicker1
        val parkingCheckBox : CheckBox = scheduleVisitBinding.parkingCheckbox
        val arrivalButton : Button = scheduleVisitBinding.arrival

        currentDate = SimpleDateFormat("yyyyMMdd").format(calendarView.date)

        calendarView.setOnDateChangeListener { view: CalendarView, year: Int, month: Int, dayOfMonth: Int ->
                currentDate = year.toString()
                if (month < 9){
                    currentDate += "0"
                }
                currentDate += (month+1).toString()
                if (dayOfMonth < 10){
                    currentDate += "0"
                }
                currentDate += dayOfMonth.toString()
            }

        arrivalButton.setOnClickListener{

            currentHour = timePicker.hour
            currentMinute = timePicker.minute
            currentTime = ""
            if (currentHour < 10){
                currentTime = "0"
            }
            currentTime += currentHour.toString()
            if (currentMinute < 10){
                currentTime += "0"
            }
            currentTime += currentMinute.toString()
            parking = parkingCheckBox.isChecked

            if (isOnline(this)){
                addVisit()
            }
            else{
                val newFragment = RetryDialogFragment()
                newFragment.show(supportFragmentManager, "No connection")
            }
        }

        backButton.setOnClickListener{
            finish()
        }

        /**
         * Here comes the buttom bar
         */

        homeButton.setOnClickListener{
            val mIntent = Intent(this@ScheduleVisitActivity, HomeActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        paymentsButton.setOnClickListener{
            val mIntent = Intent(this@ScheduleVisitActivity, PaymentsActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        servicesButton.setOnClickListener{
            val mIntent = Intent(this@ScheduleVisitActivity, ServicesActivity::class.java)
            mIntent.putExtra("token", token)
            mIntent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            startActivity(mIntent)
        }

        visitorsButton.setOnClickListener{
            finish()
        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        Toast.makeText(applicationContext, "Volvimos al scheduler!", Toast.LENGTH_SHORT).show()
        var sharedPref : SharedPreferences = getPreferences(MODE_PRIVATE)
        val editor = sharedPref.edit()
        var offlineCounter = sharedPref.getInt("SCHEDULE-OFFLINE-COUNTER", 0)
        if(isOnline(this)){
            vm.addOnline(token, 1, "visits")
            for(i: Int in 0 until offlineCounter){
                vm.addOnline(token, 0, "visits")
            }
            editor.putInt("SCHEDULE-OFFLINE-COUNTER", 0)
        }
        else {
            editor.putInt("SCHEDULE-OFFLINE-COUNTER", offlineCounter+1)
        }

        editor.apply()
    }

    private fun addVisit(){
        vm.postScheduledVisit(token, firstName, secondName, parking, currentDate, currentTime)
        vm.responseScheduledVisit?.observe(this, Observer {
            val dto = it ?: return@Observer
            var message = "Couldn't schedule " +  firstName + " for a visit"
            if(dto.detail == "Visit created succesfully"){
                message = firstName + " scheduled for a visit!"
            }
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
            finish()
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val capabilities =
            connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
        if (capabilities != null) {
            when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                }
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }

        return false
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDialogPositiveClick(dialog: DialogFragment) {
        if (isOnline(this)){
            addVisit()
        }
        else{
            val newFragment = RetryDialogFragment()
            newFragment.show(supportFragmentManager, "No connection")
        }
    }

    override fun onDialogNegativeClick(dialog: DialogFragment) {

    }
}