import androidx.collection.LruCache
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.data.responseDTOs.ResponsePaymentDTO

class NewResidentLruCache constructor() {

    private object HOLDER {
        val INSTANCE = NewResidentLruCache()
    }

    companion object {

        val instance: NewResidentLruCache by lazy { HOLDER.INSTANCE }
    }

    val lru: LruCache<Any, Any> = LruCache(1)

    fun saveNewResidentToCache(key: String, residentDTO: newResidentDTO) {

        try {
            NewResidentLruCache.instance.lru.put(key, residentDTO)
        } catch (e: Exception) {
        }

    }

    fun retrieveNewResidentFromCache(key: String): newResidentDTO? {

        try {
            return NewResidentLruCache.instance.lru.get(key) as newResidentDTO?
        } catch (e: Exception) {
        }

        return null
    }

    fun getRealSize1(): Int{
        return NewResidentLruCache.instance.lru.size()
    }

}