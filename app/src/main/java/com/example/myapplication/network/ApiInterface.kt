package com.example.myapplication.network

import com.example.myapplication.data.DTOs.ScheduledVisitDTO
import com.example.myapplication.data.DTOs.UserLoginDTO
import com.example.myapplication.data.DTOs.newResidentDTO
import com.example.myapplication.data.DTOs.newVisitorDTO
import com.example.myapplication.data.responseDTOs.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @POST("/users/{user}")
    fun getUserAuthentication(@Path("user") user : String, @Body body: UserLoginDTO): Call<ResponseUserLoginDTO>

    @POST("/visitor/{token}")
    fun uploadNewVisitor(@Path("token") token: String, @Body body: newVisitorDTO): Call<ResponseNewVisitorDTO>

    @GET("create/resident/{code}")
    fun verifyCode(@Path("code") code: String): Call<ResponseVerificationCodeDTO>

    @POST("create/resident")
    fun createNewResident(@Body body: newResidentDTO): Call<ResponseNewResidentDTO>

    @GET("/near_unpaid_payments/{token}")
    fun getUnpaidPayments(@Path("token") token : String): Call<List<ResponsePaymentDTO>>

    @GET("/payments/{token}")
    fun getPayments(@Path("token") token : String): Call<List<ResponsePaymentDTO>>

    @GET("/cacheamount/{token}/android/{amount}")
    fun addCacheAmount(@Path("amount") amount : Int, @Path("token") token : String): Call<ResponseCacheAmountDTO>

    @GET("/online/{token}/android/{value}/{view}")
    fun addOnline(@Path("token") token : String, @Path("value") value : Int, @Path("view") view : String): Call<ResponseOnlineViewDTO>

    @GET("/visitors/frequent/{token}")
     fun getFrequentVisitors(@Path("token") token : String): Call<List<ResponseVisitorsDTO>>

    @GET("/visitors/nofrequent/{token}")
     fun getLastMonthsVisitors(@Path("token") token : String): Call<List<ResponseVisitorsDTO>>

    @GET("/visitors/last3months/{token}")
     fun getDeleteRecomVisitors(@Path("token") token : String): Call<List<ResponseVisitorsDTO>>

    @GET("/authtimes/{token}/android/{time}")
    fun uploadAuthTime(@Path("token") token : String, @Path("time") time : String ): Call<ResponseAddTimeAuthDTO>

    @GET("/register_time/android/{time}")
    fun uploadCreateResidentTime(@Path("time") time : String ): Call<ResponseAddTimeCreateResidentDTO>

    @GET("/notifications/{token}/android/{req}")
    fun uploadNotification(@Path("token") token: String, @Path("req") req : String): Call<ResponseNotificationDTO>

    @GET("/payments_ml_model/{token}/{month}")
    fun getMonthPrediction(@Path("token") token: String, @Path("month") month: Int): Call<ResponseMonthPredictionDTO>

    @GET("/notifications/{token}/android/{check}")
    fun uploadRememberMe(@Path("token") token: String, @Path("check") check: String): Call<ResponseRememberMeDTO>

    @POST("/visit/{token}")
    fun postScheduledVisit(@Path("token") token: String, @Body body: ScheduledVisitDTO): Call<ResponseScheduledVisit>

    @GET("/services/{token}")
    fun getServices(@Path("token") token: String): Call<List<ResponseServiceDTO>>

    @GET("/reservations/{service}/{date}")
    fun getReservations(@Path("service") service: String, @Path("date") date: String): Call<List<ResponseReservationDTO>>

    @GET("/user_id/{token}")
    fun getResidentId(@Path("token") token: String): Call<List<ResponseUserIdDTO>>

    @GET("/reservations/{token}/{service}/{date}/{hour}/{minute}")
    fun makeReservation(@Path("token") token: String, @Path("service") service: String, @Path("date") date: String, @Path("hour") hour: Int, @Path("minute") minute: Int): Call<ResponseMakeReservationDTO>

    @GET("/reservation_time/android/{time}")
    fun uploadReservationTime(@Path("time") time: String): Call<ResponseAddTimeAuthDTO>

    @GET("/favorite/{token}")
    fun getFavorite(@Path("token") token: String): Call<String>

}