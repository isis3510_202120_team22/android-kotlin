package com.example.myapplication.network
import androidx.collection.LruCache
import com.example.myapplication.data.responseDTOs.ResponseServiceDTO


class ServicesLruCache constructor() {

    private object HOLDER {
        val INSTANCE = ServicesLruCache()
    }

    companion object {

        val instance: ServicesLruCache by lazy { HOLDER.INSTANCE }
    }

    val lru: LruCache<Any, Any> = LruCache(8)

    fun saveServicesToCache(key: String, serviceDTO: ResponseServiceDTO) {

        try {
            ServicesLruCache.instance.lru.put(key, serviceDTO)
        } catch (e: Exception) {
        }

    }

    fun retrieveServicesFromCache(key: String): ResponseServiceDTO? {

        try {
            return ServicesLruCache.instance.lru.get(key) as ResponseServiceDTO?
        } catch (e: Exception) {
        }

        return null
    }

    fun getRealSize1(): Int{
        return ServicesLruCache.instance.lru.size()
    }

}