import androidx.collection.LruCache
import com.example.myapplication.data.responseDTOs.ResponsePaymentDTO

class PaymentsLruCache constructor() {

    private object HOLDER {
        val INSTANCE = PaymentsLruCache()
    }

    companion object {

        val instance: PaymentsLruCache by lazy { HOLDER.INSTANCE }
    }

    val lru: LruCache<Any, Any> = LruCache(10)

    fun savePaymentsToCache(key: String, paymentDTO: ResponsePaymentDTO) {

        try {
            PaymentsLruCache.instance.lru.put(key, paymentDTO)
        } catch (e: Exception) {
        }

    }

    fun retrievePaymentsFromCache(key: String): ResponsePaymentDTO? {

        try {
            return PaymentsLruCache.instance.lru.get(key) as ResponsePaymentDTO?
        } catch (e: Exception) {
        }

        return null
    }

    fun getRealSize1(): Int{
        return PaymentsLruCache.instance.lru.size()
    }

}