package com.example.myapplication.network

import androidx.collection.LruCache
import com.example.myapplication.data.responseDTOs.ResponseVisitorsDTO

class VisitorsLruCache {

    private object HOLDER {
        val INSTANCE = VisitorsLruCache()
    }

    companion object {
        val instance: VisitorsLruCache by lazy { HOLDER.INSTANCE }
    }

    private val frequentLru: LruCache<Any, Any> = LruCache(3)
    private val regularLru: LruCache<Any, Any> = LruCache(5)

    fun saveVisitorsToCache(key: String, visitorDTO: ResponseVisitorsDTO) {

        try {
            instance.regularLru.put(key, visitorDTO)
        } catch (e: Exception) {
        }

    }

    fun retrieveVisitorsFromCache(key: String): ResponseVisitorsDTO? {

        try {
            return instance.regularLru.get(key) as ResponseVisitorsDTO?
        } catch (e: Exception) {
        }

        return null
    }

    fun getVisitorsSize(): Int{
        return instance.regularLru.size()
    }

    fun saveFrequentVisitorsToCache(key: String, visitorDTO: ResponseVisitorsDTO) {

        try {
            instance.frequentLru.put(key, visitorDTO)
        } catch (e: Exception) {
        }

    }

    fun retrieveFrequentVisitorsFromCache(key: String): ResponseVisitorsDTO? {

        try {
            return instance.frequentLru.get(key) as ResponseVisitorsDTO?
        } catch (e: Exception) {
        }

        return null
    }

    fun getFrequentVisitorsSize(): Int{
        return instance.frequentLru.size()
    }

}